package com.otus.study.tankgame

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.KotlinObjectGenerator
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.burnFuel.BurnFuelFactory
import com.otus.study.tankgame.model.exception.PropertyReadException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest(classes = [BurnFuelFactory::class, AdapterFactory::class, KotlinObjectGenerator::class])
class BurnFuelCommandTest {

    @Autowired
    private lateinit var burnFuelFactory: BurnFuelFactory

    @Autowired
    private lateinit var adapterFactory: AdapterFactory

    @MockBean
    private lateinit var kotlinObjectGenerator: KotlinObjectGenerator

    @Test
    fun `correct gameObject parameters should lead to successful fuel combustion`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("fuel")).thenReturn(100.0)
        Mockito.`when`(tank.getProperty("fuelConsumptionRate")).thenReturn(10.0)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateBurnFuelableAdapter")

        val burnFuel = burnFuelFactory.getCommand(tank)
        burnFuel()

        Mockito.verify(tank).setProperty("fuel", 90.0)
    }

    @Test
    fun `burning fuel at an object without the 'fuel' property should throw an exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("fuel")).then {
            throw PropertyReadException("The object has the `fuel` property defined, but not set")
        }

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateBurnFuelableAdapter")

        val burnFuel = burnFuelFactory.getCommand(tank)
        Assertions.assertThrows(PropertyReadException::class.java){burnFuel()}
    }

}