package com.otus.study.tankgame.adapter.test

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.CheckFuelable
import kotlin.Double

public class GenerateCheckFuelableAdapter(gameObject: GameObject) : AbstractAdapter(gameObject), CheckFuelable {
  public override fun getFuel(): Double = getGameObject().getProperty("fuel") as kotlin.Double
  public override fun getFuelConsumptionRate(): Double = getGameObject().getProperty("fuelConsumptionRate") as kotlin.Double
}
