package com.otus.study.tankgame.adapter.test

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.BurnFuelable
import kotlin.Double
import kotlin.Unit

public class GenerateBurnFuelableAdapter(gameObject: GameObject) : AbstractAdapter(gameObject), BurnFuelable {
  public override fun getFuel(): Double = getGameObject().getProperty("fuel") as kotlin.Double

  public override fun getFuelConsumptionRate(): Double = getGameObject().getProperty("fuelConsumptionRate") as kotlin.Double

  public override fun setFuel(newFuel: Double): Unit {
    getGameObject().setProperty("fuel", newFuel)
  }

}
