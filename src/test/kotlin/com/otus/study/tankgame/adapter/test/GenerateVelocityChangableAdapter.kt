package com.otus.study.tankgame.adapter.test

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.VelocityChangable
import com.otus.study.tankgame.model.vector.RotationAngles
import com.otus.study.tankgame.model.vector.Vector
import kotlin.Unit

public class GenerateVelocityChangableAdapter(gameObject: GameObject) : AbstractAdapter(gameObject), VelocityChangable {
  public override fun setVelocity(newVelocity: Vector): Unit {
    getGameObject().setProperty("velocity", newVelocity)
  }

  public override fun getAngelesDelta(): RotationAngles =
      getGameObject().getProperty("angelesDelta") as
      com.otus.study.tankgame.model.vector.RotationAngles

  public override fun getRotationAngles(): RotationAngles =
      getGameObject().getProperty("rotationAngles") as
      com.otus.study.tankgame.model.vector.RotationAngles

  public override fun getVector(): Vector = getGameObject().getProperty("vector") as
      com.otus.study.tankgame.model.vector.Vector

  public override fun getVelocity(): Vector = getGameObject().getProperty("velocity") as
      com.otus.study.tankgame.model.vector.Vector

  public override fun setRotationAngles(newAngles: RotationAngles): Unit {
    getGameObject().setProperty("rotationAngles", newAngles)
  }

  public override fun setVector(newValue: Vector): Unit {
    getGameObject().setProperty("vector", newValue)
  }
}
