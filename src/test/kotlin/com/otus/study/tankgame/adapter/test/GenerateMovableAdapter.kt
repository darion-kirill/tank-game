package com.otus.study.tankgame.adapter.test

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.Movable
import com.otus.study.tankgame.model.vector.Vector
import kotlin.Unit

public class GenerateMovableAdapter(gameObject: GameObject) : AbstractAdapter(gameObject), Movable {
  public override fun getVector(): Vector = getGameObject().getProperty("vector") as
      com.otus.study.tankgame.model.vector.Vector

  public override fun getVelocity(): Vector = getGameObject().getProperty("velocity") as
      com.otus.study.tankgame.model.vector.Vector

  public override fun setVector(newValue: Vector): Unit {
    getGameObject().setProperty("vector", newValue)
  }
}
