package com.otus.study.tankgame.adapter.test

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.RotateWithVelocityChangable
import com.otus.study.tankgame.model.vector.Vector

public class GenerateRotateWithVelocityChangableAdapter(gameObject: GameObject) : AbstractAdapter(gameObject), RotateWithVelocityChangable {
  public override fun getVelocity(): Vector = getGameObject().getProperty("velocity") as
      com.otus.study.tankgame.model.vector.Vector
}
