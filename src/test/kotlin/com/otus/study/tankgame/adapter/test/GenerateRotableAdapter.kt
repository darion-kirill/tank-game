package com.otus.study.tankgame.adapter.test

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.Rotable
import com.otus.study.tankgame.model.vector.RotationAngles
import kotlin.Unit

public class GenerateRotableAdapter(gameObject: GameObject): AbstractAdapter(gameObject), Rotable {
  public override fun getAngelesDelta(): RotationAngles =
      getGameObject().getProperty("angelesDelta") as
      com.otus.study.tankgame.model.vector.RotationAngles

  public override fun getRotationAngles(): RotationAngles =
      getGameObject().getProperty("rotationAngles") as
      com.otus.study.tankgame.model.vector.RotationAngles

  public override fun setRotationAngles(newAngles: RotationAngles): Unit {
    getGameObject().setProperty("rotationAngles", newAngles)
  }
}
