package com.otus.study.tankgame.autogenerate

import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterAutoGeneratator
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.autogenerate.spec.FileSpecWrapper
import com.otus.study.tankgame.model.autogenerate.spec.adapter.AdapterFileSpecBuilder
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.nio.file.Paths

@SpringBootTest(classes = [AdapterAutoGeneratator::class, AdapterFactory::class, AdapterFileSpecBuilder::class ])
class AdapterAutoGeneratatorTest {

    @Autowired
    private lateinit var adapterAutoGeneratator: AdapterAutoGeneratator

    @MockBean
    private lateinit var adapterFactory: AdapterFactory

    @MockBean
    private lateinit var adapterFileSpecBuilder: AdapterFileSpecBuilder

    @Test
    fun generateObjectTest(){

        val requiredInterface = Class.forName("com.otus.study.tankgame.model.capability.Movable").kotlin
        val generatePackage = "com.otus.study.tankgame.model.adapter.generate"
        val adapterName = "Generate${requiredInterface.simpleName}Adapter"

        val fileSpecMock = Mockito.mock(FileSpecWrapper::class.java)

        Mockito.`when`(adapterFileSpecBuilder.invoke(requiredInterface, adapterName, generatePackage))
            .thenReturn(fileSpecMock)

        adapterAutoGeneratator.generateObject(requiredInterface)

        Mockito.verify(fileSpecMock).writeTo(Paths.get("src/main/kotlin"))
        Mockito.verify(adapterFactory).addAdapter("$generatePackage.$adapterName")
    }


}