package com.otus.study.tankgame.autogenerate

import com.otus.study.tankgame.model.autogenerate.spec.adapter.AdapterFileSpecBuilder
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.io.File

@SpringBootTest(classes = [AdapterFileSpecBuilder::class])
open class AdapterFileSpecBuilderTest {

    @Autowired
    private lateinit var adapterFileSpecBuilder: AdapterFileSpecBuilder

    private val testingCapabilities = mapOf(
        "GenerateBurnFuelableAdapter" to "com.otus.study.tankgame.model.capability.BurnFuelable",
        "GenerateCheckFuelableAdapter" to "com.otus.study.tankgame.model.capability.CheckFuelable",
        "GenerateMovableAdapter" to "com.otus.study.tankgame.model.capability.Movable",
        "GenerateRotableAdapter" to "com.otus.study.tankgame.model.capability.Rotable",
        "GenerateRotateWithVelocityChangableAdapter" to "com.otus.study.tankgame.model.capability.RotateWithVelocityChangable",
        "GenerateVelocityChangableAdapter" to "com.otus.study.tankgame.model.capability.VelocityChangable"
    )

    private val generatePackage = "com.otus.study.tankgame.model.adapter.generate"

    @Test
    fun invokeTest(){

        testingCapabilities.forEach{

            val adapterFileSpec = adapterFileSpecBuilder.invoke(Class.forName(it.value).kotlin, it.key, generatePackage)

            var actualString = adapterFileSpec.writeTo().replace(" ", "").replace("\n", "")
            actualString = actualString.substring(actualString.indexOf("import"))

            val expectedStringBuilder = StringBuilder()
            File("src/test/kotlin/com/otus/study/tankgame/adapter/test/${it.key}.kt")
                .forEachLine { expectedStringBuilder.append(it) }

            var expectedString = expectedStringBuilder.toString().replace(" ", "").replace("\n", "")
            expectedString = expectedString.substring(expectedString.indexOf("import"))

            Assertions.assertEquals(expectedString, actualString)
        }

    }

}