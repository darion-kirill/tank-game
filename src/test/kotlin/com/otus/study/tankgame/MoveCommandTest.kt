import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.KotlinObjectGenerator
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.command.move.MoveFactory
import com.otus.study.tankgame.model.exception.PropertyDefinitionException
import com.otus.study.tankgame.model.exception.PropertyReadException
import com.otus.study.tankgame.model.vector.Vector
import com.otus.study.tankgame.model.vector.Vector2D
import org.junit.jupiter.api.Test

import org.junit.Assert.*
import org.junit.jupiter.api.Assertions
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest(classes = [MoveFactory::class, AdapterFactory::class, KotlinObjectGenerator::class])
class MoveCommandTest {

    @Autowired
    private lateinit var moveFactory: MoveFactory

    @Autowired
    private lateinit var adapterFactory: AdapterFactory

    @MockBean
    private lateinit var kotlinObjectGenerator: KotlinObjectGenerator

    @Test
    fun `testing the correctness of changing coordinates`(){

        val tank: GameObject = Mockito.mock(GameObject::class.java)

        val coordinates: Vector = Vector2D()
        coordinates.setCoordinates(mapOf("X" to 12.0, "Y" to 5.0))
        Mockito.`when`(tank.getProperty("vector")).thenReturn(coordinates)

        val velocity: Vector = Vector2D()
        velocity.setCoordinates(mapOf("X" to -7.0, "Y" to 3.0))
        Mockito.`when`(tank.getProperty("velocity")).thenReturn(velocity)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateMovableAdapter")

        val move: Command = moveFactory.getCommand(tank)
        move()

        val expectedVector: Vector = Vector2D()
        expectedVector.setCoordinates(mapOf("X" to 5.0, "Y" to 8.0))

        val actualVector: Vector = tank.getProperty("vector") as Vector

        assertEquals(
            expectedVector,
            actualVector
        )
    }

    @Test
    fun `move of gameObject without 'position' property should throw exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("vector")).then {
            throw PropertyReadException("The object has the `vector` property defined, but not set")
        }

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateMovableAdapter")
        val move: Command = moveFactory.getCommand(tank)
        Assertions.assertThrows(PropertyReadException::class.java){move()}
    }

    @Test
    fun `move of gameObject without 'velocity' property should throw exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("velocity")).then {
            throw PropertyReadException("The object has the `velocity` property defined, but not set")
        }

        val move: Command = moveFactory.getCommand(tank)
        Assertions.assertThrows(PropertyReadException::class.java){move()}
    }

    @Test
    fun `move of gameObject without defined 'velocity' property should throw exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("velocity")).then {
            throw PropertyDefinitionException("The property `velocity` is not defined for the object")
        }

        val move: Command = moveFactory.getCommand(tank)
        Assertions.assertThrows(PropertyDefinitionException::class.java){move()}
    }

}