import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.KotlinObjectGenerator
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.command.rotate.RotateFactory
import com.otus.study.tankgame.model.exception.PropertyDefinitionException
import com.otus.study.tankgame.model.exception.PropertyReadException
import com.otus.study.tankgame.model.vector.FlatAngles
import com.otus.study.tankgame.model.vector.RotationAngles
import org.junit.jupiter.api.Test

import org.junit.Assert.*
import org.junit.jupiter.api.Assertions
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest(classes = [RotateFactory::class, AdapterFactory::class, KotlinObjectGenerator::class])
class RotateCommandTest {

    @Autowired
    private lateinit var rotateFactory: RotateFactory

    @Autowired
    private lateinit var adapterFactory: AdapterFactory

    @MockBean
    private lateinit var kotlinObjectGenerator: KotlinObjectGenerator

    @Test
    fun `testing the correctness of the rotate`(){

        val tank: GameObject = Mockito.mock(GameObject::class.java)

        val angles: RotationAngles = FlatAngles()
        angles.setAnglesOfRotation(mapOf("FLAT" to 0.0))
        Mockito.`when`(tank.getProperty("rotationAngles")).thenReturn(angles)

        val deltaAngles: RotationAngles = FlatAngles()
        angles.setAnglesOfRotation(mapOf("FLAT" to 10.0))
        Mockito.`when`(tank.getProperty("angelesDelta")).thenReturn(deltaAngles)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotableAdapter")

        val rotateCommand: Command = rotateFactory.getCommand(tank)
        rotateCommand()

        val expectedAngles: RotationAngles = FlatAngles()
        expectedAngles.setAnglesOfRotation(mapOf("FLAT" to 10.0))

        val actualAngles: RotationAngles = tank.getProperty("rotationAngles") as RotationAngles

        assertEquals(
            expectedAngles,
            actualAngles
        )
    }

    @Test
    fun `rotate of gameObject without 'angles' property should throw exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("rotationAngles")).then {
            throw PropertyReadException("The object has the `angles` property defined, but not set")
        }

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotableAdapter")
        val rotateCommand: Command = rotateFactory.getCommand(tank)
        Assertions.assertThrows(PropertyReadException::class.java){rotateCommand()}
    }

    @Test
    fun `rotate of gameObject without 'angelesDelta' property should throw exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("angelesDelta")).then {
            throw PropertyReadException("The object has the `angles_delta` property defined, but not set")
        }

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotableAdapter")
        val rotateCommand: Command = rotateFactory.getCommand(tank)
        Assertions.assertThrows(PropertyReadException::class.java){rotateCommand()}
    }


    @Test
    fun `rotate of gameObject without defined 'angelesDelta' property should throw exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("angelesDelta")).then {
            throw PropertyDefinitionException("The property `anglesDelta` is not defined for the object")
        }

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotableAdapter")
        val rotateCommand: Command = rotateFactory.getCommand(tank)
        Assertions.assertThrows(PropertyDefinitionException::class.java){rotateCommand()}
    }

}