package com.otus.study.tankgame

import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.command.MacroCommand
import com.otus.study.tankgame.model.exception.CommandException
import org.springframework.boot.test.context.SpringBootTest
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.never

@SpringBootTest
class MacroCommandTest {

    @Test
    fun `the MacroCommand must execute all nested commands`(){
        val firstCommand = Mockito.mock(Command::class.java)
        val secondCommand = Mockito.mock(Command::class.java)

        val macroCommand = MacroCommand(firstCommand, secondCommand)
        macroCommand()

        Mockito.verify(firstCommand).invoke()
        Mockito.verify(secondCommand).invoke()
    }

    @Test(expected = CommandException::class)
    fun `an error in a nested command must terminate the chain execution`(){
        val firstCommand = Mockito.mock(Command::class.java)
        val secondCommand = Mockito.mock(Command::class.java)
        val thirdCommand = Mockito.mock(Command::class.java)
        Mockito.`when`(secondCommand.invoke()).then {
            throw CommandException("Something wrong")
        }

        val macroCommand = MacroCommand(firstCommand, secondCommand, thirdCommand)
        macroCommand()

        Mockito.verify(firstCommand).invoke()
        Mockito.verify(thirdCommand, never()).invoke()
    }

}