package com.otus.study.tankgame

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.KotlinObjectGenerator
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.straightLineMoving.StraightLineMovingFactory
import com.otus.study.tankgame.model.exception.CommandException
import com.otus.study.tankgame.model.vector.Vector
import com.otus.study.tankgame.model.vector.Vector2D
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.springframework.boot.test.context.SpringBootTest
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest(classes = [StraightLineMovingFactory::class, AdapterFactory::class, KotlinObjectGenerator::class])
class StraightLineMovingCommandTest {

    @Autowired
    private lateinit var straightLineMovingFactory: StraightLineMovingFactory

    @Autowired
    private lateinit var adapterFactory: AdapterFactory

    @MockBean
    private lateinit var kotlinObjectGenerator: KotlinObjectGenerator

    @Test
    fun `the correct gameObject should move without errors`(){

        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("fuel")).thenReturn(100.0)
        Mockito.`when`(tank.getProperty("fuelConsumptionRate")).thenReturn(10.0)

        val coordinates: Vector = Vector2D()
        coordinates.setCoordinates(mapOf("X" to 12.0, "Y" to 5.0))
        Mockito.`when`(tank.getProperty("vector")).thenReturn(coordinates)

        val velocity: Vector = Vector2D()
        velocity.setCoordinates(mapOf("X" to -7.0, "Y" to 3.0))
        Mockito.`when`(tank.getProperty("velocity")).thenReturn(velocity)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateCheckFuelableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateMovableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateBurnFuelableAdapter")
        val straightLineMoving = straightLineMovingFactory.getCommand(tank)
        straightLineMoving()

        val expectedVector: Vector = Vector2D()
        expectedVector.setCoordinates(mapOf("X" to 5.0, "Y" to 8.0))

        val actualVector: Vector = tank.getProperty("vector") as Vector

        Assert.assertEquals(
                expectedVector,
                actualVector
        )

        Mockito.verify(tank).setProperty("fuel", 90.0)
    }

    @Test
    fun `insufficient fuel should throw an exception`(){

        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("fuel")).thenReturn(10.0)
        Mockito.`when`(tank.getProperty("fuelConsumptionRate")).thenReturn(100.0)

        val coordinates: Vector = Vector2D()
        coordinates.setCoordinates(mapOf("X" to 12.0, "Y" to 5.0))
        Mockito.`when`(tank.getProperty("vector")).thenReturn(coordinates)

        val velocity: Vector = Vector2D()
        velocity.setCoordinates(mapOf("X" to -7.0, "Y" to 3.0))
        Mockito.`when`(tank.getProperty("velocity")).thenReturn(velocity)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateCheckFuelableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateMovableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateBurnFuelableAdapter")
        val straightLineMoving = straightLineMovingFactory.getCommand(tank)
        Assertions.assertThrows(CommandException::class.java){straightLineMoving()}
    }

}