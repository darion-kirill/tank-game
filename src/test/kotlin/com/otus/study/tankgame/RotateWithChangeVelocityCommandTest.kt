package com.otus.study.tankgame

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.KotlinObjectGenerator
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.rotateWithChangeVelocity.RotateWithChangeVelocityFactory
import com.otus.study.tankgame.model.exception.PropertyReadException
import com.otus.study.tankgame.model.vector.FlatAngles
import com.otus.study.tankgame.model.vector.RotationAngles
import com.otus.study.tankgame.model.vector.Vector
import com.otus.study.tankgame.model.vector.Vector2D
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest(classes = [RotateWithChangeVelocityFactory::class, AdapterFactory::class, KotlinObjectGenerator::class])
class RotateWithChangeVelocityCommandTest {

    @Autowired
    private lateinit var rotateWithChangeVelocityFactory: RotateWithChangeVelocityFactory

    @Autowired
    private lateinit var adapterFactory: AdapterFactory

    @MockBean
    private lateinit var kotlinObjectGenerator: KotlinObjectGenerator

    @Test
    fun `testing the correctness of the rotation of an object with velocity`(){

        val tank: GameObject = Mockito.mock(GameObject::class.java)

        val coordinates: Vector = Vector2D()
        coordinates.setCoordinates(mapOf("X" to 0.0, "Y" to 0.0))
        Mockito.`when`(tank.getProperty("vector")).thenReturn(coordinates)

        val velocity: Vector = Vector2D()
        velocity.setCoordinates(mapOf("X" to 2.0, "Y" to 0.0))
        Mockito.`when`(tank.getProperty("velocity")).thenReturn(velocity)

        val angles: RotationAngles = FlatAngles()
        angles.setAnglesOfRotation(mapOf("FLAT" to 0.0))
        Mockito.`when`(tank.getProperty("rotationAngles")).thenReturn(angles)

        val deltaAngles: RotationAngles = FlatAngles()
        deltaAngles.setAnglesOfRotation(mapOf("FLAT" to 90.0))
        Mockito.`when`(tank.getProperty("angelesDelta")).thenReturn(deltaAngles)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateVelocityChangableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotateWithVelocityChangableAdapter")
        val rotateWithChangeVelocityCommand = rotateWithChangeVelocityFactory.getCommand(tank)
        rotateWithChangeVelocityCommand()

        val expectedVelocity: Vector = Vector2D()
        expectedVelocity.setCoordinates(mapOf("X" to 0.0, "Y" to 2.0))

        Mockito.verify(tank).setProperty("velocity", expectedVelocity)

        val expectedAngles: RotationAngles = FlatAngles()
        expectedAngles.setAnglesOfRotation(mapOf("FLAT" to 90.0))
        val actualAngles: RotationAngles = tank.getProperty("rotationAngles") as RotationAngles

        Assert.assertEquals(
                expectedAngles,
                actualAngles
        )
    }

    @Test
    fun `testing the correctness of the rotation of an object without velocity`(){

        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("velocity")).then {
            throw PropertyReadException("The object has the `velocity` property defined, but not set")
        }

        val coordinates: Vector = Vector2D()
        coordinates.setCoordinates(mapOf("X" to 0.0, "Y" to 0.0))
        Mockito.`when`(tank.getProperty("vector")).thenReturn(coordinates)

        val angles: RotationAngles = FlatAngles()
        angles.setAnglesOfRotation(mapOf("FLAT" to 0.0))
        Mockito.`when`(tank.getProperty("rotationAngles")).thenReturn(angles)

        val deltaAngles: RotationAngles = FlatAngles()
        deltaAngles.setAnglesOfRotation(mapOf("FLAT" to 90.0))
        Mockito.`when`(tank.getProperty("angelesDelta")).thenReturn(deltaAngles)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateVelocityChangableAdapter")
        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateRotateWithVelocityChangableAdapter")
        val rotateWithChangeVelocityCommand = rotateWithChangeVelocityFactory.getCommand(tank)
        rotateWithChangeVelocityCommand()

        val expectedVelocity: Vector = Vector2D()
        expectedVelocity.setCoordinates(mapOf("X" to 0.0, "Y" to 2.0))

        Mockito.verify(tank, never()).setProperty("velocity", expectedVelocity)

        val expectedAngles: RotationAngles = FlatAngles()
        expectedAngles.setAnglesOfRotation(mapOf("FLAT" to 90.0))
        val actualAngles: RotationAngles = tank.getProperty("rotationAngles") as RotationAngles

        Assert.assertEquals(
                expectedAngles,
                actualAngles
        )
    }

}