package com.otus.study.tankgame

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.KotlinObjectGenerator
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.checkFuel.CheckFuelFactory
import com.otus.study.tankgame.model.exception.PropertyReadException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest(classes = [CheckFuelFactory::class, AdapterFactory::class, KotlinObjectGenerator::class])
class CheckFuelCommandTest {

    @Autowired
    private lateinit var checkFuelFactory: CheckFuelFactory

    @Autowired
    private lateinit var adapterFactory: AdapterFactory

    @MockBean
    private lateinit var kotlinObjectGenerator: KotlinObjectGenerator

    @Test
    fun `an adequate supply of fuel must be checked`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("fuel")).thenReturn(100.0)
        Mockito.`when`(tank.getProperty("fuelConsumptionRate")).thenReturn(10.0)

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateCheckFuelableAdapter")

        val checkFuel = checkFuelFactory.getCommand(tank)
        checkFuel()

        Mockito.verify(tank).getProperty("fuel")
        Mockito.verify(tank).getProperty("fuelConsumptionRate")
    }

    @Test
    fun `insufficient fuel should throw an exception`(){
        val tank: GameObject = Mockito.mock(GameObject::class.java)
        Mockito.`when`(tank.getProperty("fuel")).then {
            throw PropertyReadException("The object has the `fuel` property defined, but not set")
        }

        adapterFactory.addAdapter("com.otus.study.tankgame.adapter.test.GenerateCheckFuelableAdapter")

        val checkFuel = checkFuelFactory.getCommand(tank)
        Assertions.assertThrows(PropertyReadException::class.java){checkFuel()}
    }


}