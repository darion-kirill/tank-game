package com.otus.study.tankgame.model.command

import com.otus.study.tankgame.model.exception.CommandException

interface Command {
    @Throws(CommandException::class)
    operator fun invoke()
}