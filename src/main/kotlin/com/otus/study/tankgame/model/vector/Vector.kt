package com.otus.study.tankgame.model.vector

interface Vector {
    fun getCoordinates(): Map<String, Double>
    fun setCoordinates(newCoordinates: Map<String, Double>)
    operator fun plusAssign(velocity: Vector)
}