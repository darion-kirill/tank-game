package com.otus.study.tankgame.model.exception

import java.io.IOException

class PropertyReadException(override val message: String): IOException(message)