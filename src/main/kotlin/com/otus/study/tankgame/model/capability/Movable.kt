package com.otus.study.tankgame.model.capability

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.vector.Vector

interface Movable {
    fun getVector(): Vector
    fun setVector(newValue: Vector)
    fun getVelocity(): Vector
    fun getGameObject(): GameObject
}