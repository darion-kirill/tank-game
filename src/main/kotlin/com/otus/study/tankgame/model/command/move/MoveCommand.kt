package com.otus.study.tankgame.model.command.move

import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.Movable
import com.otus.study.tankgame.model.command.Command

class MoveCommand(private val abstractAdapter: AbstractAdapter) : Command {

    override fun invoke(){
        val movable = abstractAdapter as Movable
        movable.getVector() += movable.getVelocity()
    }

}