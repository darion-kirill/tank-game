package com.otus.study.tankgame.model.command.rotate

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.command.CommandFactory
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn("kotlinObjectGenerator")
class RotateFactory(private val adapterFactory: AdapterFactory): CommandFactory {

    companion object{
        val INTERFACE_NAME = "Rotable"
    }

    override fun getCommand(gameObject: GameObject): Command {
        val adapter = adapterFactory.getAdapter(INTERFACE_NAME, gameObject)
        return RotateCommand(adapter)
    }
}