package com.otus.study.tankgame.model.command.straightLineMoving

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.command.CommandFactory
import com.otus.study.tankgame.model.command.MacroCommand
import com.otus.study.tankgame.model.command.burnFuel.BurnFuelCommand
import com.otus.study.tankgame.model.command.checkFuel.CheckFuelCommand
import com.otus.study.tankgame.model.command.move.MoveCommand
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn("kotlinObjectGenerator")
class StraightLineMovingFactory(private val adapterFactory: AdapterFactory): CommandFactory {

    override fun getCommand(gameObject: GameObject): Command {

        val checkFuelCommand = CheckFuelCommand(adapterFactory.getAdapter("CheckFuelable", gameObject))
        val moveCommand = MoveCommand(adapterFactory.getAdapter("Movable", gameObject))
        val burnFuelCommand = BurnFuelCommand(adapterFactory.getAdapter("BurnFuelable", gameObject))

        return MacroCommand(checkFuelCommand, moveCommand, burnFuelCommand)
    }

}