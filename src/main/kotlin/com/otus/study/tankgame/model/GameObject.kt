package com.otus.study.tankgame.model

import com.otus.study.tankgame.model.exception.PropertyDefinitionException
import com.otus.study.tankgame.model.exception.PropertyReadException
import com.otus.study.tankgame.model.vector.Vector
import kotlin.jvm.Throws

interface GameObject {

    @Throws(PropertyReadException::class)
    fun getProperty(key: String): Any

    @Throws(PropertyDefinitionException::class)
    fun setProperty(key: String, any: Any)

}