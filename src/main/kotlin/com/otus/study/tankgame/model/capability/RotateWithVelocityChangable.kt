package com.otus.study.tankgame.model.capability

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.vector.Vector

interface RotateWithVelocityChangable {
    fun getVelocity(): Vector
    fun getGameObject(): GameObject
}