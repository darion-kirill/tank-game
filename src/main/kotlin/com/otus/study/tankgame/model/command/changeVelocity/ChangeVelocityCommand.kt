package com.otus.study.tankgame.model.command.changeVelocity

import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.VelocityChangable
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.vector.Vector2D
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin
import kotlin.math.sqrt

class ChangeVelocityCommand(private val abstractAdapter: AbstractAdapter): Command {

    override fun invoke() {

        val velocityChangable = abstractAdapter as VelocityChangable

        val newVectorLength = sqrt((velocityChangable.getVelocity().getCoordinates().get("X") as Double).pow(2.0) +
                (velocityChangable.getVelocity().getCoordinates().get("Y") as Double).pow(2.0))

        val deltaAngles = velocityChangable.getAngelesDelta().getAnglesOfRotation().getOrDefault("FLAT", 0.0)
        val newVector = Vector2D()

        val newX = Math.round(newVectorLength * cos(Math.toRadians(deltaAngles)) * 1000.0) / 1000.0
        val newY = Math.round(newVectorLength * sin(Math.toRadians(deltaAngles)) * 1000.0) / 1000.0

        newVector.setCoordinates(mapOf("X" to newX, "Y" to newY))

        velocityChangable.setVelocity(newVector)
    }

}