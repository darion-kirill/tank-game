package com.otus.study.tankgame.model.command.burnFuel

import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.BurnFuelable
import com.otus.study.tankgame.model.command.Command

class BurnFuelCommand(private val abstractAdapter: AbstractAdapter): Command {
    override fun invoke(){
        val burnFuelable = abstractAdapter as BurnFuelable
        burnFuelable.setFuel(burnFuelable.getFuel() - burnFuelable.getFuelConsumptionRate())
    }
}