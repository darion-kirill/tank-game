package com.otus.study.tankgame.model.capability

import com.otus.study.tankgame.model.vector.Vector

interface VelocityChangable: Movable, Rotable {
    fun setVelocity(newVelocity: Vector)
}