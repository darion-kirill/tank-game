package com.otus.study.tankgame.model.capability

import com.otus.study.tankgame.model.GameObject

interface CheckFuelable {
    fun getFuel(): Double
    fun getFuelConsumptionRate(): Double
    fun getGameObject(): GameObject
}