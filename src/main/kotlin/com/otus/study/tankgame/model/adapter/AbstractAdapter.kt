package com.otus.study.tankgame.model.adapter

import com.otus.study.tankgame.model.GameObject

open class AbstractAdapter(private val gameObject: GameObject) {

    fun getGameObject(): GameObject = this.gameObject

}