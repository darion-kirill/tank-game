package com.otus.study.tankgame.model.autogenerate.generator.adapter

import com.otus.study.tankgame.model.autogenerate.spec.adapter.AdapterFileSpecBuilder
import com.otus.study.tankgame.model.autogenerate.generator.AutoGenerator
import java.nio.file.Paths
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct
import kotlin.reflect.KClass

@Component
open class AdapterAutoGeneratator(private val adapterFactory: AdapterFactory,
                                  private val adapterFileSpecBuilder: AdapterFileSpecBuilder
): AutoGenerator {

    private val outPath = Paths.get("src/main/kotlin")
    private val generatePackage = "com.otus.study.tankgame.model.adapter.generate"

    @PostConstruct
    override fun clearSourcePackage() {
        val packageToClear = Paths.get("${outPath}/${generatePackage.replace(".", "/")}").toFile()
        if (packageToClear.isDirectory){
            packageToClear.listFiles()?.forEach { it.delete() }
        }
    }

    override fun getSourcePackage() = "com.otus.study.tankgame.model.capability"

    override fun generateObject(requiredInterface: KClass<*>) {
        val adapterName = "Generate${requiredInterface.simpleName}Adapter"
        val adapter = adapterFileSpecBuilder.invoke(requiredInterface, adapterName, generatePackage)
        adapter.writeTo(outPath)
        adapterFactory.addAdapter("$generatePackage.$adapterName")
    }

}