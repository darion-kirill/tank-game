package com.otus.study.tankgame.model.vector

import java.util.HashMap

class FlatAngles: RotationAngles {

    private var flatAngel = 0.0

    override fun getAnglesOfRotation(): Map<String, Double> {
        val currentAngles: MutableMap<String, Double> = HashMap()
        currentAngles["FLAT"] = flatAngel
        return currentAngles
    }

    override fun setAnglesOfRotation(newAngles: Map<String, Double>) {
        flatAngel = newAngles.getOrDefault("FLAT", flatAngel)
    }

    override fun plusAssign(deltaAngles: RotationAngles) {
        flatAngel = flatAngel.plus(deltaAngles.getAnglesOfRotation().getOrDefault("FLAT", 0.0))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FlatAngles

        if (flatAngel != other.flatAngel) return false

        return true
    }

    override fun hashCode(): Int {
        return flatAngel.hashCode()
    }

}