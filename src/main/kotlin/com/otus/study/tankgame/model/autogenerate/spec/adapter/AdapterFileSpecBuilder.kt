package com.otus.study.tankgame.model.autogenerate.spec.adapter

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.autogenerate.spec.FileSpecWrapper
import com.squareup.kotlinpoet.*
import org.springframework.stereotype.Service
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.functions

@Service
open class AdapterFileSpecBuilder {

    open fun invoke(requiredInterface: KClass<*>, adapterName: String, generatePackage: String): FileSpecWrapper {

        val adapterFunctions = getAdapterFunction(requiredInterface)

        val adapterTypeSpec = TypeSpec.classBuilder(adapterName)
            .addSuperinterface(requiredInterface)
            .superclass(AbstractAdapter::class)
            .addSuperclassConstructorParameter("gameObject", GameObject::class)
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter("gameObject", GameObject::class)
                    .build()
            )
            .addFunctions(adapterFunctions)
            .build()

        return AdapterFIleSpecWrapper(
            FileSpec.builder(generatePackage, adapterName).addType(adapterTypeSpec).build()
        )
    }

    private fun getAdapterFunction(requiredInterface: KClass<*>): MutableList<FunSpec> {
        return requiredInterface.functions
            .filter {
                it.name != "equals" && it.name != "toString" && it.name != "hashCode" && it.name != "getGameObject"
            }
            .map { func ->
                val funcSpec = FunSpec
                    .builder(func.name)
                    .addModifiers(KModifier.OVERRIDE)

                val objectParamName =
                    func.name.substring(3, 4).lowercase(Locale.getDefault()) + func.name.substring(4)

                if (func.name.contains("get")) {
                    funcSpec.returns(func.returnType.asTypeName())
                        .addStatement("return getGameObject().getProperty(\"$objectParamName\") as ${func.returnType}")
                } else if (func.name.contains("set")) {
                    func.parameters
                        .filter { it.name != null }
                        .forEach {
                            val funcParamName: String = it.name.let { name -> name.toString() }
                            funcSpec.addParameter(funcParamName, it.type.asTypeName())
                                .addStatement("getGameObject().setProperty(\"$objectParamName\", $funcParamName)")
                        }
                } else {
                    funcSpec
                        .addStatement(
                            "throw com.otus.study.tankgame.model.exception.CommandException(%S)",
                            "Implementation of the METHOD function is not defined in the AUTO object"
                        )
                }
                funcSpec.build()
            }
            .toMutableList()
    }

}