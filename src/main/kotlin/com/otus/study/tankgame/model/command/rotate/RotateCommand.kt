package com.otus.study.tankgame.model.command.rotate

import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.Rotable
import com.otus.study.tankgame.model.command.Command

class RotateCommand(private val abstractAdapter: AbstractAdapter): Command {

    override fun invoke(){
        val rotable = abstractAdapter as Rotable
        rotable.getRotationAngles() += rotable.getAngelesDelta()
    }

}