package com.otus.study.tankgame.model.capability

import com.otus.study.tankgame.model.GameObject

interface BurnFuelable {
    fun getFuel(): Double
    fun setFuel(newFuel: Double)
    fun getFuelConsumptionRate(): Double
    fun getGameObject(): GameObject
}