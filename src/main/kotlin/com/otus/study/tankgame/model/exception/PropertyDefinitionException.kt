package com.otus.study.tankgame.model.exception

import java.io.IOException

class PropertyDefinitionException(override val message: String): IOException(message)