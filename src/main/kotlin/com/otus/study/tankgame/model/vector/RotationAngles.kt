package com.otus.study.tankgame.model.vector

interface RotationAngles {
    fun getAnglesOfRotation(): Map<String, Double>
    fun setAnglesOfRotation(newAngles: Map<String, Double>)
    operator fun plusAssign(newAngles: RotationAngles)
}