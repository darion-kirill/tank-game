package com.otus.study.tankgame.model.command

class MacroCommand(vararg commands: Command) : Command{

    private var commands: MutableList<Command> = mutableListOf()

    init {
        commands.forEach { command -> this.commands.add(command) }
    }

    override fun invoke() = this.commands.forEach { it.invoke() }

}