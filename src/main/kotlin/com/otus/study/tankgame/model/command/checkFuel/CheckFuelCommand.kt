package com.otus.study.tankgame.model.command.checkFuel

import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.capability.CheckFuelable
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.exception.CommandException

class CheckFuelCommand(private val abstractAdapter: AbstractAdapter): Command {

    override fun invoke() {
        val checkFuelable = abstractAdapter as CheckFuelable
        if (checkFuelable.getFuel() - checkFuelable.getFuelConsumptionRate() < 0){
            throw CommandException("Fuel supply is insufficient")
        }
    }

}