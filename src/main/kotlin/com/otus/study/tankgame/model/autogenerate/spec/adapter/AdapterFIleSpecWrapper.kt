package com.otus.study.tankgame.model.autogenerate.spec.adapter

import com.otus.study.tankgame.model.autogenerate.spec.FileSpecWrapper
import com.squareup.kotlinpoet.FileSpec
import java.lang.StringBuilder
import java.nio.file.Path

class AdapterFIleSpecWrapper(private var fileSpec: FileSpec): FileSpecWrapper {

    override fun writeTo(outPath: Path) {
        fileSpec.writeTo(outPath)
    }

    override fun writeTo(): String {
        val temp = StringBuilder()
        fileSpec.writeTo(temp)
        return temp.toString()
    }

}