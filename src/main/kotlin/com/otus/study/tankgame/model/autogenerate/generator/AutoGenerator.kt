package com.otus.study.tankgame.model.autogenerate.generator

import kotlin.reflect.KClass

interface AutoGenerator {
    fun generateObject(requiredInterface: KClass<*>)
    fun clearSourcePackage()
    fun getSourcePackage(): String
}