package com.otus.study.tankgame.model.command

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.exception.CommandException
import org.springframework.stereotype.Component
import java.util.*
import kotlin.jvm.Throws

@Component
class CommandFactoryFacade(private val commands: List<CommandFactory>) {

    @Throws(CommandException::class)
    fun getCommand(commandName: String, gameObject: GameObject): Command{

        return commands.stream()
            .filter{ it::class
                .simpleName
                ?.lowercase(Locale.getDefault())
                .equals(commandName.lowercase(Locale.getDefault()))
            }
            .findFirst()
            .orElseThrow { throw CommandException("$commandName command not found") }
            .getCommand(gameObject)
    }

}