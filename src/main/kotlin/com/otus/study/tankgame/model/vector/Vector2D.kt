package com.otus.study.tankgame.model.vector

import java.util.HashMap

class Vector2D: Vector {

    private var x: Double = 0.0
    private var y: Double = 0.0

    override fun getCoordinates(): Map<String, Double> {
        val currentCoordinates: MutableMap<String, Double> = HashMap()
        currentCoordinates["X"] = x
        currentCoordinates["Y"] = y
        return currentCoordinates
    }

    override fun setCoordinates(newCoordinates: Map<String, Double>) {
        x = newCoordinates.getOrDefault("X", this.x)
        y = newCoordinates.getOrDefault("Y", this.y)
    }

    override fun plusAssign(velocity: Vector){
        this.x = this.x.plus(velocity.getCoordinates().getOrDefault("X", 0.0))
        this.y = this.y.plus(velocity.getCoordinates().getOrDefault("Y", 0.0))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vector2D

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

}