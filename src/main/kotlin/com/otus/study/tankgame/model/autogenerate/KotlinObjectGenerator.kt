package com.otus.study.tankgame.model.autogenerate

import com.google.common.collect.ImmutableSet
import com.google.common.reflect.ClassPath
import com.otus.study.tankgame.model.autogenerate.generator.AutoGenerator
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
open class KotlinObjectGenerator(private var autoGenerators: List<AutoGenerator>){

    @PostConstruct
    operator fun invoke() {
        autoGenerators.forEach { generator ->
            val capabilities = loadClassesFromPackage(generator.getSourcePackage())
            capabilities.forEach { generator.generateObject(Class.forName(it.name).kotlin) }
        }
    }

    private fun loadClassesFromPackage(requiredPackage: String): ImmutableSet<ClassPath.ClassInfo> {
        return ClassPath
            .from(ClassLoader.getSystemClassLoader())
            .getTopLevelClasses(requiredPackage)
    }

}