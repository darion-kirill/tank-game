package com.otus.study.tankgame.model.command.rotateWithChangeVelocity

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.capability.RotateWithVelocityChangable
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.command.CommandFactory
import com.otus.study.tankgame.model.command.MacroCommand
import com.otus.study.tankgame.model.command.changeVelocity.ChangeVelocityCommand
import com.otus.study.tankgame.model.command.rotate.RotateCommand
import com.otus.study.tankgame.model.exception.PropertyReadException
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn("kotlinObjectGenerator")
class RotateWithChangeVelocityFactory(private val adapterFactory: AdapterFactory): CommandFactory {

    override fun getCommand(gameObject: GameObject): Command {

        val rotableAdapter = adapterFactory.getAdapter("Rotable", gameObject)
        val rotateWithVelocityChangable = adapterFactory
            .getAdapter("RotateWithVelocityChangable", gameObject) as RotateWithVelocityChangable

        if (hasObjectVelocity(rotateWithVelocityChangable)){
            val velocityChangableAdapter = adapterFactory.getAdapter("VelocityChangable", gameObject)
            val macroCommand = MacroCommand(
                ChangeVelocityCommand(velocityChangableAdapter),
                RotateCommand(rotableAdapter)
            )
            return macroCommand
        } else{
            return RotateCommand(rotableAdapter)
        }
    }

    private fun hasObjectVelocity(rotateWithVelocityChangable: RotateWithVelocityChangable): Boolean{
        return try {
            rotateWithVelocityChangable.getVelocity()
            true
        }catch (e: PropertyReadException){
            false
        }
    }

}