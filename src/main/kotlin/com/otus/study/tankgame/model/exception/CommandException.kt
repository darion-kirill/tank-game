package com.otus.study.tankgame.model.exception

import java.io.IOException

class CommandException(override val message: String): IOException(message)