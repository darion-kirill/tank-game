package com.otus.study.tankgame.model.autogenerate.generator.adapter

import com.otus.study.tankgame.model.adapter.AbstractAdapter
import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.exception.CommandException
import org.springframework.stereotype.Component
import kotlin.reflect.full.primaryConstructor

@Component
open class AdapterFactory{

    private var adapters: MutableList<String> = mutableListOf()

    @Throws(CommandException::class)
    fun getAdapter(capabilityName: String, gameObject: GameObject): AbstractAdapter {
        val adapter = adapters.stream().filter { it.contains(capabilityName) }.findFirst().orElseThrow {
            throw CommandException("No adapter found for capability $capabilityName")
        }
        val adapterClass = Class.forName(adapter).kotlin

        return adapterClass.primaryConstructor?.call(gameObject) as AbstractAdapter
    }

    open fun addAdapter(packageName: String){
        adapters.add(packageName)
    }

}