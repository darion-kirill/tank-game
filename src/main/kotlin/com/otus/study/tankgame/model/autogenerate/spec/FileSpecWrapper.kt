package com.otus.study.tankgame.model.autogenerate.spec

import java.nio.file.Path

interface FileSpecWrapper {
    fun writeTo(outPath: Path)
    fun writeTo(): String
}