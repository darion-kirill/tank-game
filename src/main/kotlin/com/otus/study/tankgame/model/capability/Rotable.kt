package com.otus.study.tankgame.model.capability

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.vector.RotationAngles

interface Rotable {
    fun getRotationAngles(): RotationAngles
    fun setRotationAngles(newAngles: RotationAngles)
    fun getAngelesDelta(): RotationAngles
    fun getGameObject(): GameObject
}