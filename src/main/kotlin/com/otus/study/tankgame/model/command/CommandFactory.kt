package com.otus.study.tankgame.model.command

import com.otus.study.tankgame.model.GameObject

interface CommandFactory {
    fun getCommand(gameObject: GameObject): Command
}