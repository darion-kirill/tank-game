package com.otus.study.tankgame.model.command.move

import com.otus.study.tankgame.model.GameObject
import com.otus.study.tankgame.model.autogenerate.generator.adapter.AdapterFactory
import com.otus.study.tankgame.model.command.Command
import com.otus.study.tankgame.model.command.CommandFactory
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn("kotlinObjectGenerator")
class MoveFactory(private val adapterFactory: AdapterFactory): CommandFactory {

    companion object{
        val INTERFACE_NAME = "Movable"
    }

    override fun getCommand(gameObject: GameObject): Command {
        val adapter = adapterFactory.getAdapter(INTERFACE_NAME, gameObject)
        return MoveCommand(adapter)
    }
}