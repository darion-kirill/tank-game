package com.otus.study.tankgame

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TankGameApplication

fun main(args: Array<String>) {
	runApplication<TankGameApplication>(*args)
}
